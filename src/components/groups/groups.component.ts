import { Component, OnInit } from '@angular/core'
import { MatDialog } from '@angular/material/dialog'

import { GroupsService, NotifierService, SessionService } from '../../services'
import { Group } from '../../vendor/shared'
import { GroupDialogComponent } from '../dialogs/group/group.dialog.component'
import stubs from '../../vendor/lang/group.lang'

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss'],
})
export class GroupsComponent implements OnInit {
  isFetching = false
  groups: Group[] = []
  sessionData
  errors = {
    fetchError: false,
    createError: false,
    removeError: false,
  }

  constructor(
    private groupsService: GroupsService,
    private notifierService: NotifierService,
    private newGroupDialog: MatDialog,
    private sessionService: SessionService,
  ) {}

  ngOnInit() {
    this.isFetching = true
    this.sessionData = this.sessionService.getSession()
    this.groupsService.fetchGroups(this.sessionData._id).subscribe(
      groups => {
        this.groups = groups || []
        this.isFetching = false
      },
      () => {
        this.notifierService.openSnackBar(stubs.ru.errors.fetch, stubs.ru.accept)
        this.errors.fetchError = true
        this.isFetching = false
      },
    )
  }

  onOpenNewEventDialog() {
    this.newGroupDialog.open(GroupDialogComponent, { data: {} })
  }

  onEditGroup(groupId: string) {}

  onOpenRemoveDialog(groupId: string) {}
}
