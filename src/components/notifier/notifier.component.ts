import { Component } from '@angular/core'
import { MatSnackBar } from '@angular/material/snack-bar'

@Component({
  selector: 'app-notifier',
  templateUrl: './notifier.component.html',
  styleUrls: ['./notifier.component.scss'],
})
export class NotifierComponent {
  constructor(private snackBar: MatSnackBar) {}

  openSnackBar(message: string, action: string, duration = 2000) {
    this.snackBar.open('blaMessage', 'bla action', {
      duration,
    })
  }
}
