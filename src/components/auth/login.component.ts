import {Component, OnInit} from '@angular/core'
import {Validators, FormGroup, FormControl} from '@angular/forms'
import {Router} from '@angular/router'
import {UsersService} from '../../services/users.service'
import {users as langs} from '@lang'
import {Lang} from '../../vendor/shared'

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup
    curLang = Lang.en
    isLoginMode = true
    wrongCredentials = false
    APIError = false

    constructor(private usersService: UsersService, private router: Router) {
    }

    ngOnInit() {
        this.initForm()
    }

    initForm() {
        const username = ''
        const password = ''

        this.loginForm = new FormGroup({
            username: new FormControl(username, [
                Validators.required,
                Validators.maxLength(50),
            ]),
            password: new FormControl(password, [
                Validators.required,
                Validators.minLength(6),
                Validators.maxLength(50),
            ]),
        })
    }

    onSwitchMode() {
        this.isLoginMode = !this.isLoginMode
    }

    onSubmit() {
        this.usersService.login(this.loginForm.value).subscribe(
            res => {
                localStorage.setItem('accessToken', res.accessToken)
                this.router.navigate(['pages/events'])
            },
            err => {
                const {status} = err
                if (status === 401) {
                    return (this.wrongCredentials = true)
                }
                return (this.APIError = true)
            },
        )
    }
}
