import { Component, OnInit, OnDestroy } from '@angular/core'
import { MatDialog } from '@angular/material/dialog'
import { Subscription } from 'rxjs'

import {
  ConfirmService,
  EventsService,
  NotifierService,
  TogglerService,
  SessionService,
} from '../../services'
import { NewEventDialogComponent } from '../dialogs/new-event-dialog/new-event-dialog.component'
import { ConfirmDialogComponent } from '../dialogs/confirm/confirm-dialog.component'
import { IEvent } from '../../vendor/shared'
import stubs from '../../vendor/lang/event-list.lang'

@Component({
  selector: 'app-posts',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss'],
})
export class EventListComponent implements OnInit, OnDestroy {
  events: IEvent[] = []
  isFetching = false
  displayedColumns: string[] = ['thumb', 'title', 'deliveryDate', 'action', 'status']
  errors = {
    fetchError: false,
    createError: false,
    removeError: false,
  }
  sessionData: any
  confirmRemoveItem$: Subscription

  constructor(
    private togglerService: TogglerService, // to toggle sidebar
    private notifierService: NotifierService,
    private eventsService: EventsService,
    private confirmService: ConfirmService,
    private newEventDialog: MatDialog,
    private confirmDialog: MatDialog,
    private sessionService: SessionService,
  ) {}

  ngOnInit() {
    this.isFetching = true
    this.eventsService.newEvent.subscribe(event => {
      // some actions here
    })
    this.eventsService.fetchEvents(this.sessionService.getSession()._id).subscribe(
      events => {
        this.events = events || []
        this.isFetching = false
      },
      () => {
        this.notifierService.openSnackBar(stubs.ru.errors.onFetchEvents, stubs.ru.notifier.accept)
        this.errors.fetchError = true
      },
    )
    this.eventsService.newEvent.subscribe(event => {
      this.events.push(event)
      this.notifierService.openSnackBar(stubs.ru.notifier.eventAdded, stubs.ru.notifier.accept)
    })
    this.eventsService.createError.subscribe(truthy => (this.errors.createError = truthy))
  }

  ngOnDestroy() {
    // this.fetchError.unsubscribe()
  }

  onRemoveEvent(id: string): boolean {
    this.eventsService.removeEvent(id).subscribe(
      deleteRes => {
        const filteredEvents = this.events.filter(event => event._id !== id)
        // this.events = filteredEvents
      },
      () => {
        this.notifierService.openSnackBar(stubs.ru.errors.onDeleteEvent, stubs.ru.notifier.accept)
      },
    )
    return true
  }

  onOpenNewEventDialog() {
    this.newEventDialog.open(NewEventDialogComponent, { data: {} })
  }

  onOpenRemoveEventDialog(id: string) {
    const dialogRef = this.confirmDialog.open(ConfirmDialogComponent, {
      data: stubs.ru.confirmDialog,
    })
    console.log('confirm event')
    this.confirmService.confirmEventRemove.subscribe(() => {
      this.eventsService.removeEvent(id).subscribe(
        () => {
          this.notifierService.openSnackBar(stubs.ru.notifier.eventRemoved, stubs.ru.notifier.accept)
          this.events = this.events.filter(event => event._id !== id)
        },
        err => this.notifierService.openSnackBar(stubs.ru.errors.onDeleteEvent, stubs.ru.notifier.accept),
      )
    })
    dialogRef.afterClosed().subscribe(() => {})
    // subscribe for an confirm event action
    // on close modal unsubscribe
  }
}
