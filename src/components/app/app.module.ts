import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { registerLocaleData } from '@angular/common'
import {
  MatIconModule,
  MatToolbarModule,
  MatSidenavModule,
  MatListModule,
  MatDialogModule,
  MatInputModule,
  MatSnackBarModule,
  MatGridListModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatTableModule,
} from '@angular/material'
import { AuthInterceptorService } from '../../services/auth-interceptor.service'
import { MatButtonModule } from '@angular/material/button'
import { UrlInterceptorService } from '../../services'
import { UsersModule } from '../users/users.module'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import localeEn from '@angular/common/locales/en'
import localeRu from '@angular/common/locales/ru'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { EventListComponent } from '../events/event-list.component'
import { ConversionComponent } from '../conversion/conversion.component'
import { FilterPipe } from '../../pipes/filter.pipe'
import { EventTitlePipe } from '../../pipes/event-title.pipe'
import { StringifyPipe } from '../../pipes/stringify.pipe'
import { NewEventDialogComponent } from '../dialogs/new-event-dialog/new-event-dialog.component'
import { EventDeliverPipe } from '../../pipes/event-delivery.pipe'
import { NotifierComponent } from '../notifier/notifier.component'
import { ConfirmDialogComponent } from '../dialogs/confirm/confirm-dialog.component'
import { LoginComponent } from '../auth/login.component'
import { PagesComponent } from '../../pages/pages.component'
import { GroupsComponent } from '../groups/groups.component'
import { ReceiversComponent } from '../receivers/receivers.component'
import { GroupDialogComponent } from '../dialogs/group/group.dialog.component'

registerLocaleData(localeEn)
registerLocaleData(localeRu)

@NgModule({
  declarations: [
    AppComponent,
    EventListComponent,
    ConversionComponent,
    FilterPipe,
    EventTitlePipe,
    StringifyPipe,
    NewEventDialogComponent,
    EventDeliverPipe,
    NotifierComponent,
    ConfirmDialogComponent,
    LoginComponent,
    PagesComponent,
    GroupsComponent,
    ReceiversComponent,
    GroupDialogComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatDatepickerModule,
    MatDialogModule,
    MatGridListModule,
    MatInputModule,
    MatListModule,
    MatIconModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatTableModule,
    MatToolbarModule,
    ReactiveFormsModule,
    UsersModule,
  ],
  entryComponents: [NewEventDialogComponent, ConfirmDialogComponent],
  providers: [
    MatDatepickerModule,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: UrlInterceptorService, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
