import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { EventListComponent } from '../events/event-list.component'
import { PagesComponent } from '../../pages/pages.component'
import { GroupsComponent } from '../groups/groups.component'
import { ReceiversComponent } from '../receivers/receivers.component'
import { LoginComponent } from '../auth/login.component'

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  {
    path: 'pages',
    component: PagesComponent,
    children: [
      { path: 'events', component: EventListComponent },
      { path: 'groups', component: GroupsComponent },
      { path: 'receivers', component: ReceiversComponent },
      { path: 'new', component: EventListComponent },
      { path: 'edit/:id', component: EventListComponent },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
