import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core'
import * as jwtDecode from 'jwt-decode'

import { TogglerService } from '../../services/toggler.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit {
  // @ViewChild('drawer', { static: false }) drawer
  serverStatus = new Promise(r => setTimeout(() => r('online'), 2000))

  constructor(private togglerService: TogglerService) {}

  ngOnInit() {
  }

  ngAfterViewInit() {
    // this.togglerService.drawerEl = this.drawer
    // this.onToggleSidebar()
  }

  // onToggleSidebar() {
  //   this.togglerService.drawerEl.toggle()
  // }
}
