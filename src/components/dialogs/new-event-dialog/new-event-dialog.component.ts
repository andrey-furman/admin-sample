import {Component, Inject, Optional, OnInit} from '@angular/core'
import {MAT_DIALOG_DATA} from '@angular/material/dialog'
import {Validators, FormGroup, FormControl} from '@angular/forms'
import {MatDatepickerInputEvent} from '@angular/material'
import * as jwtDecode from 'jwt-decode'
import {Lang} from '../../../vendor/shared'

import {newBroadcast} from '@lang'
import {EventsService} from '../../../services'
import {prepareDeliveryDate} from '../../../vendor/utils'

export interface DialogData {
    name: string
}

@Component({
    selector: 'app-new-event',
    templateUrl: './new-event-dialog.component.html',
    styleUrls: ['./new-event-dialog.component.scss'],
})
export class NewEventDialogComponent implements OnInit {
    eventForm: FormGroup
    eventFormStub: any
    sessionData: any
    curLang: Lang.en

    constructor(
        @Optional() @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private eventsService: EventsService,
    ) {
    }

    ngOnInit() {
        this.eventFormStub = newBroadcast.ru.forms.eventForm
        this.initForm()
        this.sessionData = jwtDecode(localStorage.accessToken)._doc
    }

    private initForm() {
        const title = ''
        const thumbUrl = ''
        const body = ''
        const scheduleDate = ''
        const scheduleTime = ''

        this.eventForm = new FormGroup({
            title: new FormControl(title, [
                Validators.required,
                Validators.minLength(this.eventFormStub.tolerances.title.minLength),
                Validators.maxLength(this.eventFormStub.tolerances.title.maxLength),
            ]),
            thumbUrl: new FormControl(thumbUrl),
            body: new FormControl(body, [
                Validators.required,
                Validators.minLength(this.eventFormStub.tolerances.body.minLength),
                Validators.maxLength(this.eventFormStub.tolerances.body.maxLength),
            ]),
            scheduleDate: new FormControl(scheduleDate, [Validators.required]),
            scheduleTime: new FormControl(scheduleTime, [Validators.required]),
        })
        this.eventForm.statusChanges.subscribe(status => {
        })
    }

    // TODO prevet past Dates to be selected
    myFilter = (d: Date | null): boolean => {
        // const day = (d || new Date()).getDay();
        // return day !== 0 && day !== 6;
        return true
    }

    public onSubmit() {
        const formData = this.eventForm.value
        const {scheduleDate: date, scheduleTime} = formData
        const scheduledDate = prepareDeliveryDate(date, scheduleTime)
        this.eventsService.createEvent({...formData, scheduledDate, userId: this.sessionData._id})
        this.eventForm.reset()
    }

    public dateChange(date: MatDatepickerInputEvent<any>) {
        // TODO make some noise
    }

    public timeChange(time: string) {
        const splited = time.split(':')
        const hour = splited[0]
        const minute = splited[1]
    }
}
