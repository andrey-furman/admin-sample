import { Component, Inject, OnInit, Optional } from '@angular/core'
import { MAT_DIALOG_DATA } from '@angular/material/dialog'

import { ConfirmService } from '../../../services'

interface IDialogData {
  header: string
  action: string
  cancel: string
}

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss'],
})
export class ConfirmDialogComponent implements OnInit {
  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: IDialogData,
    private confirmService: ConfirmService,
  ) {}

  ngOnInit() {
    this.data.header = this.data.header || 'Подтвердите дейвствие'
    this.data.action = this.data.action || 'Подтвердить'
    this.data.cancel = 'Отмена'
  }

  onConfirmEvent() {
    this.confirmService.onConfirmEventRemove()
  }
}
