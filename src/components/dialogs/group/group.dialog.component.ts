import { Component, OnInit, Inject, Optional } from '@angular/core'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { MAT_DIALOG_DATA } from '@angular/material/dialog'

import { Group } from '../../../vendor/shared'
import { GroupsService } from '../../../services/groups.service'
import stubs from '../../../vendor/lang/dialogs/group-dialog.lang'

@Component({
  selector: 'app-group',
  templateUrl: './group.dialog.component.html',
  styleUrls: ['./group.dialog.component.scss'],
})
export class GroupDialogComponent implements OnInit {
  private groupForm: FormGroup

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Group,
    private groupsService: GroupsService,
  ) {}

  ngOnInit() {
    this.initForm()
  }

  private initForm() {
    let group = ''

    this.groupForm = new FormGroup({
      group: new FormControl(group, [
        Validators.required,
        Validators.minLength(stubs.ru.form.tolerances.group.minlength),
        Validators.maxLength(stubs.ru.form.tolerances.group.maxlength),
      ]),
    })
  }

  public onSubmit() {
    const formData = this.groupForm.value
    this.groupsService.create(formData)
    this.groupForm.reset()
  }
}
