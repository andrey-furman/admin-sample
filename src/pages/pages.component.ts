import { Component, ViewChild, OnInit } from '@angular/core'
import * as jwtDecode from 'jwt-decode'

import { TogglerService, SessionService } from '../services'

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss'],
})
export class PagesComponent implements OnInit {
  @ViewChild('drawer', { static: false }) drawer
  private sessionData

  constructor(private togglerService: TogglerService, private sessionService: SessionService) {}

  ngOnInit() {
    this.sessionData = this.sessionService.getSession()
  }

  ngAfterViewInit() {
    this.togglerService.drawerEl = this.drawer
    this.onToggleSidebar()
  }

  onToggleSidebar() {
    this.togglerService.drawerEl.toggle()
  }
}
