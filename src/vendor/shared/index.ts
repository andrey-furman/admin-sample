export * from './event-partition'

export const defaultTime = '09:00'

export interface Group {
    group: string
    userId: string
    createdAt: Date
    updatedAt: Date
}

export interface User {
    id: number,
    email: string,
    firstName: string,
    lastName: string,
    password: string
    accessToken?: string
}

export enum Lang {
    en = 'en',
    ru = 'ru'
}
