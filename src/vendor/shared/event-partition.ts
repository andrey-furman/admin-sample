export interface IEvent {
  _id?: string
  thumbUrl: string
  title: string
  views: number
  status: string
  scheduledDate: Date
  body: string
  createdAt: Date
  updatedAt: Date
}
