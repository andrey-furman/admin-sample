export const eventList = {
    ru: {
        confirmDialog: {
            header: 'Удалить событие',
            action: 'Удалить',
        },
        notifier: {
            accept: 'Принять',
            eventAdded: '✅Событие добавлено',
            eventRemoved: '✅Событие удалено',
        },
        errors: {
            onDeleteEvent: '🚫Ошибка при  удалении события',
            onFetchEvents: '🚫Ошибка при загрузке событий',
        },
    },
    en: {
        confirmDialog: {
            header: 'Remove event',
            action: 'Remove',
        },
        notifier: {
            accept: 'Accept',
            eventAdded: '✅Event added',
            eventRemoved: '✅Event removed',
        },
        errors: {
            onDeleteEvent: '🚫Error on remove event',
            onFetchEvents: '🚫Error on loading events',
        },
    },
}
