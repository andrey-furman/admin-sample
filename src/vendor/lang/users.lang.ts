export const users = {
    ru: {
        header: 'Авторизация',
        footer: '© Админ панель Bots Business 2020',
        loginPlaceholder: 'Логин',
        passwordPlaceholder: 'Пароль',
        enter: 'Войти',
        signUp: 'К регистрации',
        form: {
            login: {},
            password: {},
            errors: {
                required: 'Поле обязательно',
                minLength: (length: number) => `Минимальная длина - ${length} символ(а)`,
                maxLength: (length: number) => `Максимальная длина - ${length} символ(а)`,
                credentials: 'Неверное имя или пароль',
                apiError: 'Ошибка сервера. Обратитесь к администрации или повторите позже.',
            },
        },
    },
    en: {
        header: 'Authorization',
        footer: '© Admin panel Bots Business 2021',
        loginPlaceholder: 'Login',
        passwordPlaceholder: 'Password',
        enter: 'Login',
        signUp: 'To registration',
        form: {
            login: {},
            password: {},
            errors: {
                required: 'Required field',
                minLength: (length: number) => `Min length - ${length} symbol(s)`,
                maxLength: (length: number) => `Max length - ${length} symbol(s)`,
                credentials: 'Wrong username or password',
                apiError: 'Server error. Try later or contact an administrator.',
            },
        },
    },
}
