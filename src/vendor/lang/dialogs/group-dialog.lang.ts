export const groupDialog = {
  ru: {
    form: {
      tolerances: {
        group: {
          minlength: 3,
          maxlength: 155,
        },
      },
      errors: {
        common: {
          required: 'Поле обязятельное для заполнения'
        }
      }
    },
  },
  en: {},
}
