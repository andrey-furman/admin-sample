export const newBroadcast = {
    ru: {
        forms: {
            eventForm: {
                tolerances: {
                    title: {
                        maxLength: 250,
                        minLength: 5,
                    },
                    body: {
                        minLength: 10,
                        maxLength: 4096,
                    },
                },
                errors: {
                    common: {
                        invalidValue: 'Неверное значение поля.',
                        required: 'Это поле обязательно',
                        minLength: (length: number) => `Минимальная длина - ${length} символ(а).`,
                        maxLength: (length: number) => `Максимальная длина - ${length} символ(а).`,
                    },
                    newEvent: {
                        errors: {},
                    },
                },
            },
        },
    },
    en: {},
}
