export const group = {
  ru: {
    accept: 'Принять',
    errors: {
      fetch: '🚫Ошибка при получении данных. Попробуйте позже или обратитесь к администратору.'
    }
  },
  en: {
    accept: 'Accept',
    errors : {
      fetch: '🚫Error on fetching data. Try please later or contact administrator.'
    }
  }
}
