import moment from 'moment'
import {defaultTime} from '../shared';

export const prepareDeliveryDate = (date: string, time: string = defaultTime): string => {
    const formattedDate = moment(date).format('l')
    return moment(`${formattedDate} ${time}`).format('h:mm, MM/DD/YYYY')
}
