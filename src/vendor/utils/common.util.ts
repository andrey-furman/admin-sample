import moment from 'moment'

export const combineDateTime = (date: string, time: string): string => {
    return moment(`${time} ${date}`).format('h:mm, MM/DD/YYYY')
}
