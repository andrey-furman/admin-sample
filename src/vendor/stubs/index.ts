import {User} from '../shared'
// * password encrypted using md5 hash function
export const users: User[] = [{
    email: 'john.gold@gmail.com',
    firstName: 'John',
    id: 1,
    lastName: 'Gold',
    password: '5ebe2294ecd0e0f08eab7690d2a6ee69',
},
    {
        id: 2,
        email: 'jane.gold@gmail.com',
        firstName: 'Jane',
        lastName: 'Gold',
        password: '5ebe2294ecd0e0f08eab7690d2a6ee69',
    },
]
