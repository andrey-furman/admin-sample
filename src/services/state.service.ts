import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root',
})
export class StateService {
  modalStates: { newPost: { isShown: false } }

  constructor() {}

  toggleModal(modal) {
    this.modalStates[modal].isShown = !this.modalStates[modal].isShown
  }
}
