import md5 from 'md5'
import {
    HttpClient,
} from '@angular/common/http'
import {Injectable} from '@angular/core'
import {users as usersLang} from '@lang'
import {users} from '../vendor/stubs'
import {Observable} from 'rxjs'
import {User, Lang} from '../vendor/shared'

type Credentials = {
    email: string
    password: string
}

@Injectable({
    providedIn: 'root',
})
export class UsersService {
    curLang: Lang = Lang.en

    constructor(private httpClient: HttpClient) {
    }

    login(creds: Credentials): Observable<User> {
        const user = users.find(u => u.email === creds.email && u.password === md5(creds.password))
        if (!user) {
            throw new Error(usersLang[this.curLang].form.errors.credentials)
        }

        return new Observable<User>(s => {
            s.next(user)
            s.complete()
        })
    }
}
