import { Injectable } from '@angular/core'
import { Subject } from 'rxjs'

@Injectable({
  providedIn: 'root',
})
// Wrong business login graining
// TODO Remove this service at all
export class ConfirmService {
  confirmEventRemove = new Subject<boolean>()

  constructor() {}

  onConfirmEventRemove() {
    this.confirmEventRemove.next(true)
  }
}
