import { Injectable } from '@angular/core'
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpHeaders,
} from '@angular/common/http'
import { Observable } from 'rxjs'
import { environment } from '../environments/environment'

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authorizationData = environment.apiToken
    if (localStorage.accessToken) {
      authorizationData = `Bearer ${localStorage.accessToken}`
    }
    const updatedReq = req.clone({
      setHeaders: { Authorization: authorizationData },
    })

    return next.handle(updatedReq)
  }
}
