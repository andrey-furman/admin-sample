import { Injectable } from '@angular/core'
import * as jwtDecode from 'jwt-decode'

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  private sessionData

  constructor() {
    this.sessionData = jwtDecode(localStorage.accessToken)._doc
  }

  getSession() {
    return this.sessionData || {}
  }
}
