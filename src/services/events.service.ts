import { Injectable } from '@angular/core'
import {
  HttpClient,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpEvent,
} from '@angular/common/http'
import { Observable, throwError, Subject } from 'rxjs'
import { map, catchError } from 'rxjs/operators'

import { environment } from '../environments/environment'

interface IEvent {
  title: string
  body: string
  thumbUrl: string
  status: string
  views: number
  scheduledDate: Date
  createdAt: Date
  updatedAt: Date
}

@Injectable({
  providedIn: 'root',
})
export class EventsService {
  public newEvent = new Subject<any>()
  public createError = new Subject<boolean>()
  public fetchError = new Subject<boolean>()

  constructor(private httpClient: HttpClient) {}

  createEvent(eventData: IEvent): void {
    this.httpClient
      .post<IEvent>(`post`, eventData, {
        headers: { Authorization: environment.apiToken },
      })
      .subscribe(
        createRes => {
          this.newEvent.next(createRes)
        },
        err => {
          this.createError.next(true)
        },
      )
  }

  fetchEvents(userId: string): Observable<IEvent[]> {
    return this.httpClient
      .get<IEvent[]>(`post/${userId}`, {
      })
      .pipe(
        catchError(err => {
          return throwError(err)
        }),
      )
  }

  removeEvent(id: string): Observable<any> {
    return this.httpClient
      .delete(`post/${id}`, {
        headers: { Authorization: environment.apiToken },
      })
      .pipe(
        catchError(err => {
          return throwError(err)
        }),
      )
  }
}
