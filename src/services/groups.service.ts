import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Subject, Observable, throwError } from 'rxjs'
import { catchError } from 'rxjs/operators'

import { environment } from '../environments/environment'
import { Group } from '../vendor/shared'

@Injectable({
  providedIn: 'root',
})
export class GroupsService {
  public newGroup = new Subject<any>()
  public fetchError = new Subject<boolean>()
  public createError = new Subject<boolean>()

  constructor(private httpClient: HttpClient) {}

  create(groupData: Group) {
    this.httpClient
      .post<Group>('group', groupData, {
        headers: { Authorization: environment.apiToken },
      })
      .subscribe(
        createRes => {
          this.newGroup.next(createRes)
        },
        err => {
          this.createError.next(true)
        },
      )
  }

  fetchGroups(userId: string): Observable<Group[]> {
    return this.httpClient
      .get<Group[]>(`group/${userId}`)
      .pipe(
        catchError(err => {
          return throwError(err)
        }),
      )
  }
}
