import {
  HttpClient,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpEvent,
} from '@angular/common/http'
import { Injectable } from '@angular/core'

import { environment } from '../environments/environment'

@Injectable({
  providedIn: 'root',
})
export class UrlInterceptorService implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const url = `${environment.apiUrl}${req.url}`
    const apiReq = req.clone({ url })
    return next.handle(apiReq)
  }

  constructor(private httpClient: HttpClient) {}
}
