import { Injectable } from '@angular/core';

export  class TranslationSet {
  public language:  string
  public values: {[key: string]: string} = {}
}

@Injectable({
  providedIn: 'root'
})
export class TranslationService {
  public languages = ['ru', 'en']
  public language = 'ru'
  private dictionary: {[key: string]: TranslationSet} = {}

  constructor() {}
}
