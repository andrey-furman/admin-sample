import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'eventTitle',
})
export class EventTitlePipe implements PipeTransform {
  transform(value: any) {
    return value.substr(0, 10).concat('...')
  }
}
