import moment from 'moment'
import {Pipe, PipeTransform} from '@angular/core'

@Pipe({
    name: 'eventDelivery',
})
export class EventDeliverPipe implements PipeTransform {
    transform(value: any, ...args: any[]): string {
        return moment(value).format('h:mm, MM/DD/YYYY')
    }
}
